package com.gewuyou.addressbookmanagementsystem;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.gewuyou.addressbookmanagementsystem.activity.AddActivity;
import com.gewuyou.addressbookmanagementsystem.activity.interfaces.APPActivity;
import com.gewuyou.addressbookmanagementsystem.adapter.ContactAdapter;
import com.gewuyou.addressbookmanagementsystem.data.dao.ContactDao;
import com.gewuyou.addressbookmanagementsystem.data.database.manager.DataBaseManager;
import com.gewuyou.addressbookmanagementsystem.data.entity.Contact;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class MainActivity extends AppCompatActivity implements APPActivity {
    private static final DataBaseManager dataBaseManager = DataBaseManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        finishViewData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        finishViewData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 关闭数据库
        dataBaseManager.closeDatabase();
    }

    @Override
    public void initView() {
        ListView listView = this.findViewById(R.id.book_list);
        FloatingActionButton fab = findViewById(R.id.add);
        fab.setOnClickListener(v -> {
            // 打开添加界面
            Intent intent = new Intent(this, AddActivity.class);
            startActivity(intent);
            finish();
        });
        // 搜素框
        EditText searchBox = findViewById(R.id.search_box);
        //搜索按钮
        findViewById(R.id.search).setOnClickListener(v -> {
            // 获取输入内容
            String nameOrPhone = searchBox.getText().toString();
            if (nameOrPhone.isEmpty()) {
                Toast.makeText(MainActivity.this, "输入不能为空，请检查输入!", Toast.LENGTH_SHORT).show();
            }
            listView.setAdapter(null);
            List<Contact> contactsByNameOrPhone = ContactDao.getContactsByNameOrPhone(nameOrPhone);
            listView.setAdapter(new ContactAdapter(this, contactsByNameOrPhone));
        });
    }

    @Override
    public void finishViewData(boolean isRefresh) {
        // ignore
    }

    private void finishViewData() {
        // 获取导航条
        Toolbar toolBar = this.findViewById(R.id.toolbar);
        this.setSupportActionBar(toolBar);
        dataBaseManager.initDatabase(getApplicationContext());
        List<Contact> contacts = ContactDao.getAll();
        ListView listView = this.findViewById(R.id.book_list);
        if (contacts.isEmpty()) {
            listView.setAdapter(null);
        } else {
            ContactAdapter contactAdapter = new ContactAdapter(this, contacts);
            listView.setAdapter(contactAdapter);
        }
    }
}